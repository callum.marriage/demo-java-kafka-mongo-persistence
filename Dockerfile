FROM adoptopenjdk/openjdk11:x86_64-centos-jre-11.0.6_10
EXPOSE 5001

COPY target/ms-ukspf-application-persistence*.jar /submission-persistence.jar

ENTRYPOINT ["java", "-jar",  "/submission-persistence.jar"]
