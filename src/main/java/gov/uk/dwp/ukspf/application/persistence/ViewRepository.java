package gov.uk.dwp.ukspf.application.persistence;

import gov.uk.dwp.ukspf.application.persistence.model.db.DbGrantApplication;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ViewRepository extends MongoRepository<DbGrantApplication, String> {

}
