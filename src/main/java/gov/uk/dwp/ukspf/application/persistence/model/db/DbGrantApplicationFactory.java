package gov.uk.dwp.ukspf.application.persistence.model.db;

import gov.uk.dwp.ukspf.application.persistence.model.req.GrantApplication;
import gov.uk.dwp.ukspf.application.persistence.model.req.PersonalInfo;

import java.sql.Date;

public class DbGrantApplicationFactory {

  public static DbGrantApplication createDbGrantApplication(GrantApplication grantApplication){

    PersonalInfo personalInfo = grantApplication.getPersonalInfo();
    String referenceNumber = grantApplication.getReferenceNumber();
    return new DbGrantApplication(personalInfo.getFirstName(),
        referenceNumber,
        personalInfo.getLastName(),
        personalInfo.getEmail(),
        grantApplication.getHobbies().getDescription(),
        Date.valueOf(grantApplication.getSubmissionDate()));
  }
}
