package gov.uk.dwp.ukspf.application.persistence.model.db;

import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.Date;

@Document(collection = "grant_applications")
public class DbGrantApplication {

  private String referenceNumber;

  private String lastName;

  private String email;

  private String description;

  private String firstName;

  private Date submissionDate;

  public DbGrantApplication(String firstName, String referenceNumber,
                            String lastName, String email,
                            String description, Date submissionDate) {
    this.referenceNumber = referenceNumber;
    this.lastName = lastName;
    this.email = email;
    this.firstName = firstName;
    this.description = description;
    this.submissionDate = submissionDate;
  }

  public DbGrantApplication(){

  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(String referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Date getSubmissionDate() {
    return submissionDate;
  }

  public void setSubmissionDate(Date submissionDate) {
    this.submissionDate = submissionDate;
  }
}
