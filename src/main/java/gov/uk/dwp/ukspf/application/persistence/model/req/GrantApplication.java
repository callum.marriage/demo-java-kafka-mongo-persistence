package gov.uk.dwp.ukspf.application.persistence.model.req;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class GrantApplication {

  @JsonProperty("personal-info")
  private PersonalInfo personalInfo;

  @JsonProperty("hobbies")
  private Hobbies hobbies;

  @JsonProperty("signin")
  private SignIn signIn;

  @JsonProperty("referenceNumber")
  private String referenceNumber;

  @JsonProperty("submissionDate")
  private LocalDate submissionDate;

  public GrantApplication(PersonalInfo personalInfo, Hobbies hobbies,
                          SignIn signIn, String referenceNumber,
                          LocalDate submissionDate) {
    this.personalInfo = personalInfo;
    this.hobbies = hobbies;
    this.signIn = signIn;
    this.referenceNumber = referenceNumber;
    this.submissionDate = submissionDate;
  }

  public GrantApplication(){

  }

  public PersonalInfo getPersonalInfo() {
    return personalInfo;
  }

  public void setPersonalInfo(PersonalInfo personalInfo) {
    this.personalInfo = personalInfo;
  }

  public Hobbies getHobbies() {
    return hobbies;
  }

  public void setHobbies(Hobbies hobbies) {
    this.hobbies = hobbies;
  }

  public SignIn getSignIn() {
    return signIn;
  }

  public void setSignIn(SignIn signIn) {
    this.signIn = signIn;
  }

  public String getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(String referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public LocalDate getSubmissionDate() {
    return submissionDate;
  }

  public void setSubmissionDate(LocalDate submissionDate) {
    this.submissionDate = submissionDate;
  }
}
