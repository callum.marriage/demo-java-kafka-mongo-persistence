package gov.uk.dwp.ukspf.application.persistence;

import gov.uk.dwp.ukspf.application.persistence.model.db.DbGrantApplication;
import gov.uk.dwp.ukspf.application.persistence.model.db.DbGrantApplicationFactory;
import gov.uk.dwp.ukspf.application.persistence.model.req.GrantApplication;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessageConsumer {

  private final ViewRepository viewRepository;

  public MessageConsumer(ViewRepository viewRepository){
    this.viewRepository = viewRepository;
  }

  @KafkaListener(topics = "applicationSubmitted",
      containerFactory = "kafkaListenerContainerFactory",
      groupId = "persistence")
  public void listenGroupData(GrantApplication message) {
    log.info("Received Message");
    DbGrantApplication dbGrantApplication = DbGrantApplicationFactory.createDbGrantApplication(message);
    viewRepository.insert(dbGrantApplication);
    log.info("Successfully inserted");
  }
}
